/*
 * Copyright (c) 2017, Mikael Hermansson
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the <organization>.
 * 4. Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.include
 */

#include <stddef.h>
#include <stdio.h>
#include "em_configs.h"
#include "em_event_queue.h"
#include "em_timeout.h"

#if EM_TIMEOUT_SIZE > 32
#error EM_TIMEOUT_SIZE must be less than 32
#endif

typedef struct _EmTimeout {
	uint32_t timeout_ms;
	uint32_t refresh_ms;
	EmCallback1 callback;
	void *p_data;
} EmTimeout;

static EmTimeout emTimeout[EM_TIMEOUT_SIZE];
/* set insert to end, when we insert first time this is reset to 0 */
static uint8_t insert_at = EM_TIMEOUT_SIZE - 1;
static uint8_t count = 0;
static uint32_t evTimeouts = 0;

void em_timeout_init(void)
{
	em_event_queue_init();
}

int em_timeout_add(uint32_t timeout_ms, bool repeat, EmCallback1 callback, void *p_data)
{
	ASSERT(p_data == NULL, -1);

	insert_at = (insert_at + 1) % EM_TIMEOUT_SIZE;
	/* Try insert from insert_pos */
	uint8_t i = insert_at;
	for (;i < EM_TIMEOUT_SIZE;i++) {
		if (emTimeout[i].callback == NULL) {
			emTimeout[i].callback = callback;
			emTimeout[i].p_data = p_data;
			emTimeout[i].refresh_ms = repeat ? timeout_ms : 0;
			emTimeout[i].timeout_ms = em_time_now() + timeout_ms;
			evTimeouts &= ~(1 << i);
			count++;
			/* return position where object was inserted */
			return i;
		}
	}

	/* timer can't be added cause array is full */
	if (insert_at == 0)
		return -1;

	/* try insert from begining */
	for (i = 0;i < insert_at;i++) {
		if (emTimeout[i].callback == NULL) {
			emTimeout[i].callback = callback;
			emTimeout[i].p_data = p_data;
			emTimeout[i].refresh_ms = repeat ? timeout_ms : 0;
			emTimeout[i].timeout_ms = em_time_now() + timeout_ms;
			evTimeouts &= ~(1 << i);
			count++;
			/* return position where object was inserted */
			return i;
		}
	}

	return -1;
}

bool em_timeout_remove(int id)
{
	if (id < 0 || id >= EM_TIMEOUT_SIZE || emTimeout[id].callback == NULL)
		return false;

	emTimeout[id].callback = NULL;
	emTimeout[id].p_data = NULL;
	emTimeout[id].timeout_ms = 0;
	emTimeout[id].refresh_ms = 0;
	evTimeouts &= ~(1 << id);
	insert_at = id;
	count--;

	return true;
}

/* process timout's and push it to event queue if timeout */
int em_timeout_mark_timeouts(void)
{
	uint32_t time_now_ms = em_time_now();
	int res = 0;
	for (int i = 0;i < EM_TIMEOUT_SIZE;i++) {
		EmTimeout *tim = &emTimeout[i];
		if (tim->callback == NULL)
			continue ;

		if (time_now_ms >= tim->timeout_ms)  {
			evTimeouts |= (1 << i);
			if (em_event_queue_push(tim->callback, tim->p_data)) {
				res++;
				if (!tim->refresh_ms) {
					tim->callback = NULL;
					tim->p_data = NULL;
					tim->timeout_ms = 0;
					count--;
				} else {
					tim->timeout_ms = em_time_now() + tim->refresh_ms;
				}
			}
		}
	}
	
	return res;
}

int em_timeout_count_active_timers(void)
{
	return count;
}

