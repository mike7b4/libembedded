#ifndef _ARRAY_H__
#define _ARRAY_H__
#include <stdbool.h>
#include <stdint.h>
#include "em_configs.h"
#include "em_callback.h"
typedef struct _EmArray {
    uint8_t *p_buffer;
    /* size of individual object */
    uint8_t size_of;
    uint16_t size;
    uint16_t index;
} EmArray;

bool em_array_init(EmArray *p_array, void *buffer, uint16_t size, uint8_t size_of);
bool em_array_append(EmArray *p_array, void *buffer);
bool em_array_remove(EmArray *p_array, uint16_t index);
bool em_array_foreach(EmArray *p_array, EmArrayForEachCallback cb, void *p_data);
bool em_array_peek(EmArray *p_array, uint16_t index, void **p_data);

#endif /* end of _ARRAY_H__ */

