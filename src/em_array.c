#include <stddef.h>
#include <string.h>
#include "em_array.h"

bool em_array_init(EmArray *p_array, void *buffer, uint16_t size, uint8_t size_of)
{
    ASSERT(buffer != NULL, false);
    ASSERT(p_array != NULL, false);
    p_array->p_buffer = buffer;
    p_array->size = size;
    p_array->size_of = size_of;
    p_array->index = 0;
    return true;
}

bool em_array_append(EmArray *p_array, void *buffer)
{
    ASSERT(p_array != NULL, false);
    ASSERT(buffer != NULL, false);
    if (p_array->index == p_array->size)
        return false;

    uint8_t *fromptr = buffer;
    uint8_t *toptr = p_array->p_buffer + (p_array->index * p_array->size_of);
    uint8_t size_of = p_array->size_of;
    while (size_of--) {
        *(toptr++) = *(fromptr++);
    };

    p_array->index++;
    return true;
}

bool em_array_remove(EmArray *p_array, uint16_t index)
{
    ASSERT(p_array != NULL, false);
    if (p_array->index <= index)
        return false;

    /* remove last object? */
    if (index == p_array->index - 1)
    {
        p_array->index--;
    }
    else
    {
        /* get count of objects after removed object */
        uint16_t size_of = p_array->size_of;
        uint16_t count = (p_array->index - index) * size_of;
        /* get rid of object at index and move the rest backward one index */
        memcpy(&p_array->p_buffer[index * size_of], &p_array->p_buffer[(index+1) * size_of], count);
        p_array->index--;
    }

    return true;
}

bool em_array_peek(EmArray *p_array, uint16_t index, void **p_buffer)
{
    ASSERT(p_array != NULL, false);
    ASSERT(p_buffer != NULL, false);
    if (p_array->index <= index)
        return false;

    *p_buffer = &p_array->p_buffer[index * p_array->size_of];
    return true;
}

bool em_array_foreach(EmArray *p_array, EmArrayForEachCallback cb, void *p_data)
{
    ASSERT(p_array != NULL, false);
    ASSERT(cb != NULL, false);
    uint8_t *ptr = p_array->p_buffer;
    for (int i = 0; i < p_array->index;i++)
    {
        if (!cb(ptr, i, p_data))
            return false;

        ptr += p_array->size_of;
    }

    return true;
}

