/**
 * File: emfprintf.h
 * Maintainer(s):
 * Mikael Hermansson <mike@7b4.se>
 *
 * License: BSD
 *
 * kate: space-indent on; indent-width 4; replace-tabs on; remove-trailing-spaces all;
 *
 */

#ifndef __EMFPRINTF_H__
#define __EMFPRINTF_H__
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <stddef.h>
#define EMSTREAM EmStream

typedef struct _EmStream EmStream;
typedef bool (*put_byte_func)(EmStream *stream, uint8_t character);
typedef int (*read_func)(EmStream *stream, void *bytes, size_t lenght);
typedef void (*done_func)(EmStream *stream);
typedef int (*write_func)(EmStream *stream, const void *data, size_t length);

struct _EmStream {
	/* used as lookup */
	uint8_t identifier;
	void *p_data;
	put_byte_func put_byte;
	read_func read;
	write_func write; // if NULL it will fallback to put
	done_func done;
};

/* EXAMPLE  */
/*
 * static cb_put_byte(EmStream *stream, uint8_t byte) { USART_PUT_BYTE(byte); }
 ez_emstream [1] =
 {
     { .identifier=1,
         .p_data = NULL,
         .put_byte = cb_put_byte,
         .write = NULL,
         .read = NULL,
         .open = NULL,
         .close = NULL
         }
	}
	}
*/


int em_read(EmStream *p_stream, void *data , size_t size);
int em_write(EmStream *p_stream, const void *data , size_t size);
int em_scanf(EmStream *p_stream, const char *data, ... );
int em_printf(EmStream *p_stream, const char *format, ... );
uint32_t htoi(char chars[]);

#endif /* end of __EMFPRINTF_H__ */
