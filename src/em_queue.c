/*
 * Copyright (c) 2017, Mikael Hermansson
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the <organization>.
 * 4. Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.include
 */

#include <stddef.h>
#include "em_configs.h"
#include "em_queue.h"

bool em_queue_init(EmQueue *p_queue, void *p_buffer, uint16_t size, uint8_t size_of)
{
    ASSERT(p_queue != NULL, false);
    ASSERT(p_buffer != NULL, false);

    p_queue->buffer = p_buffer;
    p_queue->size = size;
    p_queue->size_of = size_of;
    p_queue->index = 0;
    p_queue->count = 0;
    p_queue->locked = false;

    return true;
}

bool em_queue_pop(EmQueue *p_queue, void *p_buffer)
{
    ASSERT(p_queue != NULL, false);
    ASSERT(p_buffer != NULL, false);

    if (p_queue->count == 0 || p_queue->locked)
        return false;

    p_queue->locked = true;
    uint8_t *fromptr = (uint8_t *)(p_queue->buffer) + p_queue->index * p_queue->size_of;
    uint8_t *toptr = (uint8_t *)p_buffer;
    for (int i = 0;i < p_queue->size_of;i++)
        *(toptr++)= *(fromptr++);

    /* accesed by push */
    p_queue->index =((p_queue->index + 1) % p_queue->size);
    p_queue->count--;
    p_queue->locked = false;

    return true;
}

bool em_queue_peek(EmQueue *p_queue, void **peekptr)
{
    ASSERT(p_queue != NULL, false);
    ASSERT(peekptr != NULL, false);
    if (p_queue->count == 0 || p_queue->locked)
        return false;

    uint8_t *ptr = (uint8_t *)(p_queue->buffer) + p_queue->index * p_queue->size_of;
    *peekptr = ptr;
    return true;
}

bool em_queue_push(EmQueue *p_queue, void *p_buffer)
{
    ASSERT(p_queue != NULL, false);
    ASSERT(p_buffer != NULL, false);
    uint8_t *fromptr = p_buffer;
    uint8_t *toptr = NULL;
    uint8_t insert_at = 0;

    if (p_queue->count == p_queue->size || p_queue->locked)
        return false;

    p_queue->locked = true;
    insert_at = ((p_queue->count+p_queue->index) % p_queue->size);
    toptr = ((uint8_t *)p_queue->buffer) + insert_at * p_queue->size_of;
    for (int i = 0;i < p_queue->size_of;i++)
        *(toptr++)= *(fromptr++);

    p_queue->count++;
    p_queue->locked = false;

    return true;
}

