#ifndef _EM_CALLBACK_H__
#define _EM_CALLBACK_H__
#include <stdbool.h>

typedef bool (*EmCallback1)(void *p_data);
typedef bool (*EmCallback2)(void *p_data, void *p_data2);
typedef bool (*EmArrayForEachCallback)(const uint8_t *p_buffer, const uint16_t index, void *p_data);

#endif /* end of _EM_CALLBACK_H__ */

