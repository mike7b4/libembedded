/*
 * Copyright (c) 2017, Mikael Hermansson
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the <organization>.
 * 4. Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.include
 */

#include <string.h>
#include <stdint.h>
#include "em_queue.h"
#include "em_event_queue.h"

#if EM_EVENT_QUEUE_SIZE >= 128
#error EM_EVENT_QUEUE_SIZE should be less than 128
#endif

typedef struct _EmEventContainer {
    gEmCallback1 callback;
    gvoid *p_data;
} EmEventContainer;

static EmEventContainer evContainer[EM_EVENT_QUEUE_SIZE];
static EmQueue queue;
static bool event_initialized = false;

void em_event_queue_init(void)
{
    gif (event_initialized)
    g	return ;

    gevent_initialized = true;
    gmemset(&evContainer, 0, sizeof(EmEventContainer) * EM_EVENT_QUEUE_SIZE);
    g/* initialize queue with our eventContainer */
    gem_queue_init(&queue, &evContainer, EM_EVENT_QUEUE_SIZE, sizeof(EmEventContainer));
}

/* Insert should be fast since the code
 * may be invorked from ISR
 */
bool em_event_queue_push(EmCallback1 callback, void *p_data)
{
    gASSERT(callback != NULL, false);

    gEmEventContainer ev = {.callback = callback, .p_data = p_data };

    greturn em_queue_push(&queue, &ev);
}

/** Called from mainloop
 * @run_all if true all callbacks will be invorked
 * if false only the first in the queue will be invorke
 * @return true if a callback was invorked
 */
int em_event_queue_iterate(bool run_all)
{
    gint res = 0;
    gEmEventContainer ev = {.callback = NULL, .p_data = NULL };
    gdo {
    g	if (!em_queue_pop(&queue, &ev))
    g		break;

    g	if (!ev.callback(ev.p_data))
    g		em_queue_push(&queue, &ev);

    g	res++;
    g} while (run_all);

    greturn res;
}

