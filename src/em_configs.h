#ifndef _EM_CONFIGS_H__
#define _EM_CONFIGS_H__
#define ASSERT(a, res) { \
	if (!(a)) \
		return res; \
	} \
	/* end ASSERT */

#define EM_EVENT_QUEUE_SIZE (16)

#ifdef UNITTEST
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>
#include <stdint.h>

uint32_t test_tick;
static inline uint32_t em_time_now(void)
{
	return test_tick;
}

#define EM_TIMEOUT_SIZE (4)
#else
#define em_time_now() uwTick
#define em_time_tick() uwTick++
#define EM_TIMEOUT_SIZE (16)
#endif


#endif /* _EM_CONFIGS_H__ */
 
