/**
 * Filename: emfprintf.c
 *
 * Copyright 2013 - Mikael Hermansson <mike@7b4.se>
 *
 * License: BSD
 *
 * kate: indent-mode space; auto-insert-doxygen true; indent-width 4;
 */

#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include "em_printf.h"

static void _int2str(EMSTREAM *stream, int num)
{
	bool start = false;
	uint32_t tens = pow(10, sizeof(int) * 2 + 1);
	if (num < 0) {
		stream->put_byte(stream, '-');
		num = abs(num);
	} else if (num == 0) {
		stream->put_byte(stream, '0');
		return;
	}
#if DEBUG_INT2STR == 1
	printf("num=%d", num);
#endif
	while (tens > 0) {
#if DEBUG_INT2STR == 1
		unsigned int rest;
		rest = (num % tens);
		printf("rest=%d num=%d tens=%d\n", rest, num, tens);
#endif
		if (start || (num / tens)) {
			start = true;
			stream->put_byte(stream, (num / tens) + '0');
			num %= tens;
		}
		tens /= 10;
	}
}

#if EMPRINTF_USE_FLOAT==1
static void _float2str(EMSTREAM *stream, float num)
{
	int exp = 0;
	if (num < 0.0) {
		stream->put_byte(stream, '-');
		num = fabs(num);
	}

	_int2str(stream, floorf(num));
	num -= floor(num);
	stream->put_byte(stream, '.');
	_int2str(stream, floorf(num));
}
#endif

static void _int2hex(EMSTREAM *p_stream, unsigned int value, int size_of)
{
	uint8_t byte;
	uint8_t nibble;
    if (size_of == 0) {
        if (value > 0xFFFFFF)
            size_of = 32;
        else if (value > 0xFFFF)
            size_of = 24;
        else if (value > 0xFF)
            size_of = 16;
        else
            size_of = 8;
    }

    while ((size_of -= 8) >= 0) {
		byte = (uint8_t)((value & (0xFF << size_of)) >> size_of);
		nibble = (byte >> 4);
		p_stream->put_byte(p_stream, nibble < 10 ? nibble + '0' : nibble - 10 + 'A');
		nibble = (byte & 0xF);
		p_stream->put_byte(p_stream, nibble < 10 ? nibble + '0' : nibble - 10 + 'A');
	}
}

static void em_data2hexstring(EMSTREAM *p_stream, const uint8_t *data, size_t length, char separator)
{
	int i = 0;
	/* fail check */
	if (data == NULL || length > 512 || length == 0) {
		return ;
	}

	for (i = 0; i < length; i++) {
		if (separator) {
			p_stream->put_byte(p_stream, separator);
		}
		_int2hex(p_stream, data[i], false);
	}
}

/** \brief em_printf is a tiny printf implementation
 *  allowed formats:
 *  %s null terminated ASCII string as arg
 *  %d print integer string,  arg  shall be integer
 *  %0X paded hex integer shall be passed as arg
 *  %X prints non padded X integer shall be passed as arg
 *  %f float/double as arg (not working atm)
 *  %S EMSTREAM shall be passed as arg (not tested)
 *  %j print with parantes json format
 */
int em_printf(EMSTREAM *p_stream, const char *format, ...)
{
	char pad = 0;
	EMSTREAM *ep = NULL;
	char *p2 = NULL;
	uint8_t *pu2 = NULL;
	uint8_t byte = 0;
    int size_of = 0;
	va_list ptr;
	int num = 0;
	const char *p = format;
	if (p_stream == NULL || p_stream->put_byte == NULL || format == NULL) {
		return 0;
	}
	va_start(ptr, format);
	while (*p != '\0') {
		/* pad is specialcase %0X */
		if (*p == '%' || pad || size_of) {
			p++;
			switch (*p) {
				case '\0':
					/* seems user terminate with an % */
					continue;
				case '%':
					p_stream->put_byte(p_stream, *p);
					break;
				case 'j':
					p_stream->put_byte(p_stream, '"');
					p2 = va_arg(ptr, char *);
					while (p2 && *p2 != '\0') {
						p_stream->put_byte(p_stream, *p2);
						p2++;
					}
					p_stream->put_byte(p_stream, '"');
					break;
				case 's':
					p2 = va_arg(ptr, char *);
					while (p2 && *p2 != '\0') {
						p_stream->put_byte(p_stream, *p2);
						p2++;
					}
					break;
				case 'S':
					ep = va_arg(ptr, EMSTREAM *);
					while (ep != NULL && ep->read != NULL && ep->read(ep, &byte, 1) == 1) {
						p_stream->put_byte(p_stream, byte);
					}
					break;
				case 'c':
					num = va_arg(ptr, int);
					byte = num & 0xFF;
					p_stream->put_byte(p_stream, byte);
					break;
				case 'H':
					pu2 = va_arg(ptr, uint8_t *);
					num = va_arg(ptr, int);
					if (pu2) {
						em_data2hexstring(p_stream, pu2, num, pad);
					}
					pad = 0;
					break;
				case 'd':
					num = va_arg(ptr, int);
					_int2str(p_stream, num);
					break;
#if EMPRINTF_USE_FLOAT==1
				case 'f':
					num = va_arg(ptr, double);
					_float2str(p_stream, num);
					break;
#endif
				case '0':
                    pad = '0';
                    continue;
				case '1':
					size_of = 8;
					continue;
				case '2':
					size_of = 16;
					continue;
				case '4':
					size_of = 32;
					continue;
				case ':':
					/* specialcase %:X or %:H modify */
					pad = ':';
					continue;
				case ' ':
					/* specialcase %0X modify */
					pad = ' ';
					continue;
				case 'X':
					num = va_arg(ptr, unsigned int);
					_int2hex(p_stream, num, size_of);
                    size_of = 0;
					pad = 0;
					break;
			}
		} else {
			p_stream->put_byte(p_stream, *p);
		}
		p++;
	}
	va_end(ptr);

	if (p_stream->done)
		p_stream->done(p_stream);

	return 0;
}

int em_write(EMSTREAM *p_stream, const void *data, size_t size)
{
	size_t i=0;
	if (p_stream == NULL || data == NULL || (p_stream->put_byte == NULL && p_stream->write == NULL) || size == 0) {
		return 0;
	}

	if (p_stream->write == NULL) {
		for (i = 0; i < size; i++) {
			if (!p_stream->put_byte(p_stream, ((uint8_t*)data)[i])) {
				return i;
			}
		}
	} else {
		p_stream->write(p_stream, data, size);
	}

	return i;
}

int em_read(EMSTREAM *p_stream, void *data , size_t size)
{
	if (p_stream == NULL || p_stream->read == NULL || data == NULL || size == 0) {
		return 0;
	}

	return p_stream->read(p_stream, data, size);
}

uint32_t htoi(char chars[])
{
	uint8_t c = 0;
	uint32_t result = 0;
	int len = strlen(chars);
	const char *p = chars;
	int pos = len*4 - 4;
	/* we cant store bigger than uint32 */
	if (pos > 28)
		pos = 28;

	while (*p != '\0' && pos >= 0) {
		c = *p;
		if ( c >= 'a' && c <= 'f') {
			c = c - 'a' + 10;
		} else if (c >= 'A' && c <= 'F') {
			c = c - 'A' + 10;
		} else if (c >= '0' && c <= '9') {
			c -= '0';
		} else {
			return result >> (pos+4);
		}

		result |= ((c << pos) & (0xF << pos));
		pos -= 4;
		p++;
	};

	return result;
}

