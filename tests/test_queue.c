#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "em_queue.h"

#define TEST_START(title) printf("%s\n", title);
#define TEST_FAILED() printf("Test aborted\n");
#define TEST_SUCEED() printf("All tests done succefully!\n");

#define TEST_INT(title, invoke, expect) {\
	bool result = (invoke); \
	if (result == expect) {\
		printf("%s \e[32mOK\e[0m\n", title); \
	} else { \
		printf("%s %d!=%d \e[31mERROR\e[0m\n", title, expect, result); \
		TEST_FAILED(); \
		exit(-1); \
	} \
	} \


typedef struct _Container {
	uint8_t some_data;
	uint16_t some_more;
	uint8_t ugly;
	uint32_t bit32;
} Container;

Container container[3];
int main(int argc, char **argv)
{
	EmQueue queue;
	/* needed when run an OS  */
    uint8_t data[5];
	memset(&data, 0, 5);
	memset(&container, 0, sizeof(Container)*3);
	TEST_START("==== Queue test ====");

	TEST_INT("init", em_queue_init(&queue, &container, 3, sizeof(Container)), true);
	Container some = {0xff, 0xFAAA, 0xDE, 0xFBFBFBFB}; 
	Container some2 = {0x00, 0xCCCC, 0xDE, 0xB00BB00B}; 
	Container some3 = {0x11, 0xDEAD, 0xAD, 0xFBFBFBFB}; 
	Container some4 = {0x00, 0xAAAA, 0xDE, 0x00000000}; 
	Container tmp = {};
	TEST_START("==== Queue push ====");
	TEST_INT("1", em_queue_push(&queue, &some), true);
	TEST_INT("2", em_queue_push(&queue, &some2), true);
	TEST_INT("3", em_queue_push(&queue, &some3), true);
	TEST_INT("4 not pushed", em_queue_push(&queue, &some), false);
	TEST_START("==== Queue pop ====");
	TEST_INT("1", em_queue_pop(&queue, &tmp), true);
	TEST_INT("1.1 check data valid", tmp.some_data == 0xFF, true);
	TEST_INT("1.2 check data valid", tmp.some_more == 0xFAAA, true);
	/* overwrite */
	TEST_INT("2", em_queue_pop(&queue, &tmp), true);
	TEST_INT("2.1 check data valid", tmp.some_data == 0x00, true);
	TEST_INT("2.2 check data valid", tmp.some_more == 0xCCCC, true);
	TEST_INT("2.3 check data valid", tmp.bit32 == 0xB00BB00B, true);

	TEST_START("==== Queue push ====");
	TEST_INT("3", em_queue_push(&queue, &some4), true);

	TEST_START("==== Queue pop ====");
	TEST_INT("4", em_queue_pop(&queue, &tmp), true);
	TEST_INT("4.1 check data valid", tmp.some_data == 0x11, true);
	TEST_INT("4.2 check data valid", tmp.some_more == 0xDEAD, true);
	TEST_INT("4.3 check data valid", tmp.ugly == 0xAD, true);

	TEST_INT("5", em_queue_pop(&queue, &tmp), true);
	TEST_INT("5.1 check data valid", tmp.some_data == 0x00, true);
	TEST_INT("5.2 check data valid", tmp.some_more == 0xAAAA, true);
	TEST_INT("5.3 check data valid", tmp.ugly == 0xDE, true);

	TEST_INT("6 init byte queue", em_queue_init(&queue, &data, 5, sizeof(uint8_t)), true);
    for (int i = 0; i < 10; i++) {
        uint8_t byte = i+'0';
        em_queue_push(&queue, &byte);
    }

    uint8_t byte = 0xFF;
	TEST_INT("6.1 init count 10", queue.count == 5, true);
	TEST_INT("6.2 queue push not inserted" , em_queue_push(&queue, &byte), false);
	TEST_INT("6.3 Queue pop" , em_queue_pop(&queue, &byte), true);
	TEST_INT("6.4 Equal '0'" , byte == '0', true);
    byte = 0xFF;
	TEST_INT("6.5 queue push 0xFF" , em_queue_push(&queue, &byte), true);
	TEST_INT("6.6 queue push not inserted" , em_queue_push(&queue, &byte), false);
	TEST_INT("6.7 Queue pop" , em_queue_pop(&queue, &byte), true);
	TEST_INT("6.7.1 Equal '1'" , byte == '1', true);
	TEST_INT("6.8 Queue pop" , em_queue_pop(&queue, &byte), true);
	TEST_INT("6.8.1 Equal '2'" , byte == '2', true);
	TEST_INT("6.9 Queue pop" , em_queue_pop(&queue, &byte), true);
	TEST_INT("6.9.1 Equal '3'" , byte == '3', true);
    byte = 0xEE;
	TEST_INT("6.10 queue push 0xFF" , em_queue_push(&queue, &byte), true);
    byte = 0xDD;
    TEST_INT("6.11 queue push 0xDD" , em_queue_push(&queue, &byte), true);
    byte = 0xCC;
	TEST_INT("6.12 queue push 0xCC" , em_queue_push(&queue, &byte), true);


	TEST_INT("7 Queue pop" , em_queue_pop(&queue, &byte), true);
	TEST_INT("7.1 Equal '4'" , byte == '4', true);

    byte = 0xBB;
	TEST_INT("6.12 queue push 0xBB" , em_queue_push(&queue, &byte), true);

	TEST_INT("8 Queue pop" , em_queue_pop(&queue, &byte), true);
    printf("fail %X %c index %d count %d\n", byte, byte, queue.index, queue.count);
    uint8_t *p = queue.buffer;
    printf("%X:%X:%X:%X:%X\n", p[0], p[1], p[2], p[3], p[4]);
	TEST_INT("8.1 Equal 0xFF" , byte == 0xFF, true);
	TEST_INT("9 Queue pop" , em_queue_pop(&queue, &byte), true);
	TEST_INT("9.1 Equal 0xEE" , byte == 0xEE, true);
	TEST_INT("10 Queue pop" , em_queue_pop(&queue, &byte), true);
	TEST_INT("10.1 Equal 0xDD" , byte == 0xDD, true);
	TEST_INT("11 Queue pop" , em_queue_pop(&queue, &byte), true);
	TEST_INT("11.1 Equal 0xCC" , byte == 0xCC, true);


	TEST_SUCEED();

	return 0;
}

