#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include "em_configs.h"
#include "em_event_queue.h"
#include "em_timeout.h"

#define TEST_START(title) printf("%s\n", title);
#define TEST_FAILED() printf("Test aborted\n");
#define TEST_SUCCEED() printf("All tests done succefully!\n");

#define TEST_INT(title, invoke, expect) {\
	int result = (invoke); \
	if (result == expect) {\
		printf("%s \e[32mOK\e[0m\n", title); \
	} else { \
		printf("%s %d!=%d \e[31mERROR\e[0m\n", title, expect, result); \
		TEST_FAILED(); \
		exit(-1); \
	} \
	} \

uint32_t test_tick = 0;
static bool on_timeout(void *p_data)
{
	return true;
}

int main(int argc, char **argv)
{
	TEST_START("==== Timeout test ====");

	em_timeout_init();
	uint32_t timeout = em_time_now() + 1000;
	TEST_INT("1 Add", em_timeout_add(100, false, (EmCallback1)on_timeout, NULL), 0);
	TEST_INT("2 Add", em_timeout_add(200, false, (EmCallback1)on_timeout, NULL), 1);
	TEST_INT("3 Add", em_timeout_add(300, false, (EmCallback1)on_timeout, NULL), 2);
	TEST_INT("4 Add", em_timeout_add(500, false, (EmCallback1)on_timeout, NULL), 3);
	TEST_INT("5 Not added", em_timeout_add(1, false, (EmCallback1)on_timeout, NULL), -1);
	TEST_INT("6 Remove", em_timeout_remove(3), true);
	TEST_INT("7 Add", em_timeout_add(1500, false, (EmCallback1)on_timeout, NULL), 3);

	TEST_START("==== Wait for timeouts ====");
	uint32_t now = em_time_now();
	while (now < timeout) {
		test_tick++;
		now = em_time_now();
		em_timeout_mark_timeouts();
	}

	timeout = now + 499;

	TEST_INT("8 active should be 1", em_timeout_count_active_timers(), 1);
	while (now < timeout) {
		test_tick++;
		now = em_time_now();
		em_timeout_mark_timeouts();
	}

	TEST_INT("9 active should be 1", em_timeout_count_active_timers(), 1);
	test_tick++;
	em_timeout_mark_timeouts();
	TEST_INT("10 active should be 1", em_timeout_count_active_timers(), 0);

	TEST_INT("run callback timeouts", em_event_queue_iterate(true), 4);

	TEST_SUCCEED();

	return 0;
}

