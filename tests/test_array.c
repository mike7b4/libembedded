#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "em_array.h"

#define TEST_START(title) printf("%s\n", title);
#define TEST_FAILED() printf("Test aborted\n");
#define TEST_SUCEED() printf("All tests done succefully!\n");

#define TEST_INT(title, invoke, expect) {\
	bool result = (invoke); \
	if (result == expect) {\
		printf("%s \e[32mOK\e[0m\n", title); \
	} else { \
		printf("%s %d!=%d \e[31mERROR\e[0m\n", title, expect, result); \
		TEST_FAILED(); \
		exit(-1); \
	} \
	} \


typedef struct _Container {
	uint8_t some_data;
	uint16_t some_more;
	uint8_t ugly;
	uint32_t bit32;
} Container;

Container container[3];
int main(int argc, char **argv)
{
    bool res = false;
	EmArray array;
	/* needed when run an OS  */
	memset(&container, 0, sizeof(Container)*3);
	TEST_START("==== Array test ====");

	TEST_INT("init", em_array_init(&array, &container, 3, sizeof(Container)), true);
	Container some1 = {0xff, 0xFAAA, 0xDE, 0xFBFBFBFB}; 
	Container some2 = {0x00, 0xCCCC, 0xDE, 0xB00BB00B}; 
	Container some3 = {0x11, 0xDEAD, 0xAD, 0xFBFBFBFB}; 
	Container some4 = {0x00, 0xAAAA, 0xDE, 0x00000000}; 
	Container *testobj;

	TEST_START("==== Array append ====");
	TEST_INT("1", em_array_append(&array, &some1), true);
	TEST_INT("2", em_array_append(&array, &some2), true);
	TEST_INT("3", em_array_append(&array, &some3), true);
	TEST_INT("4 not appended", em_array_append(&array, &some1), false);

	TEST_START("==== Array peek ====");
    res = em_array_peek(&array, 0, (void*)&testobj);
	TEST_INT("5 get(peek) first", res, true);
    TEST_INT("5.1 validate first", testobj->some_data == some1.some_data, true);
    TEST_INT("5.2 validate first", testobj->ugly == some1.ugly, true);

    res = em_array_peek(&array, 2, (void*)&testobj);
	TEST_INT("6 get(peek) last", res, true);
    TEST_INT("6.1 validate last", testobj->some_data == some3.some_data, true);
    TEST_INT("6.2 validate last", testobj->ugly == some3.ugly, true);

	TEST_START("==== Array remove ====");
	TEST_INT("7 remove first", em_array_remove(&array, 0), true);

	TEST_START("==== Array peek ====");
    res = em_array_peek(&array, 0, (void*)&testobj);
	TEST_INT("8 get(peek) first", res, true);
    TEST_INT("8.1 validate first", testobj->some_data == some2.some_data, true);
    TEST_INT("8.2 validate first", testobj->ugly == some2.ugly, true);

	TEST_START("==== Array append ====");
	TEST_INT("9", em_array_append(&array, &some1), true);

	TEST_START("==== Array peek ====");
    res = em_array_peek(&array, 2, (void*)&testobj);
	TEST_INT("10 get(peek) first", res, true);
    TEST_INT("10.1 validate first", testobj->some_data == some1.some_data, true);
    TEST_INT("10.2 validate first", testobj->ugly == some1.ugly, true);

	
	TEST_SUCEED();

	return 0;
}

