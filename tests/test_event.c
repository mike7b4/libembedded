#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "em_event_queue.h"
#define TEST_START(title) printf("%s\n", title);
#define TEST_FAILED() printf("Test aborted\n");
#define TEST_SUCEED() printf("All tests done succefully!\n");

#define TEST_INT(title, invoke, expect) {\
	int result = invoke; \
	if (result == expect) {\
		printf("%s \e[32mOK\e[0m\n", title); \
	} else { \
		printf("%s %d!=%d \e[31mERROR\e[0m\n", title, expect, result); \
		TEST_FAILED(); \
		exit(-1); \
	} \
	} \
	/* end makro */

typedef struct _Container {
	uint32_t value;
} Container;

static bool cb_test1(void *p_data)
{
	return true;
}

static bool cb_test2(Container *p_data)
{
	int was = p_data->value;
	p_data->value = !was;
	return was;
}

int
main(int argc, char **argv)
{
	Container cont = {.value = false };
	em_event_queue_init();

	TEST_START("=========== TEST EVENT QUEUE push ===========");

	TEST_INT("Test 1 Push", em_event_queue_push((EmCallback1)cb_test1, NULL), true);
	TEST_INT("Test 2 Push", em_event_queue_push((EmCallback1)cb_test1, NULL), true);
	TEST_INT("Test 3 Push", em_event_queue_push((EmCallback1)cb_test2, &cont), true);

	TEST_START("=========== TEST EVENT QUEUE iterate ===========");
	TEST_INT("Test 1 Iterate", em_event_queue_iterate(false) == 1, true);
	TEST_INT("Test 2 Iterate", em_event_queue_iterate(false) == 1, true);
	TEST_INT("Test 3 Iterate", em_event_queue_iterate(false) == 1, true);
	TEST_INT("Test 4 check pdata", cont.value, true);
	TEST_INT("Test 5 Iterate", em_event_queue_iterate(false) == 1, true);
	TEST_INT("Test 6 Iterate", em_event_queue_iterate(false) == 0, true);

	TEST_START("=========== TEST EVENT QUEUE iterate all ===========");
	em_event_queue_push((EmCallback1)cb_test1, NULL);
	em_event_queue_push((EmCallback1)cb_test1, NULL);
	em_event_queue_push((EmCallback1)cb_test1, NULL);
	em_event_queue_push((EmCallback1)cb_test1, NULL);
	TEST_INT("Test 1 Iterate all", em_event_queue_iterate(true), 4);
	TEST_INT("Test 2 Iterate all when empty", em_event_queue_iterate(true), 0);

	TEST_SUCEED();
	return 0;
}

